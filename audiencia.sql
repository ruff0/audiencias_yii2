-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 03, 2017 at 01:01 PM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `audiencia_advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `audiencia`
--

CREATE TABLE IF NOT EXISTS `audiencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha` date DEFAULT NULL,
  `hora_real` time DEFAULT NULL,
  `incidencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `tipo_audiencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_sala` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_sala` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `causa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oficio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_termino` time DEFAULT NULL,
  `tipo` tinyint(4) DEFAULT NULL,
  `hora_salida` time DEFAULT NULL,
  `hora_regreso` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oficio_2` (`oficio`),
  KEY `oficio` (`oficio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=234 ;

--
-- Dumping data for table `audiencia`
--

INSERT INTO `audiencia` (`id`, `fecha_hora`, `fecha`, `hora_real`, `incidencia`, `cancelado`, `tipo_audiencia`, `id_sala`, `tipo_sala`, `causa`, `oficio`, `hora_inicio`, `hora_termino`, `tipo`, `hora_salida`, `hora_regreso`) VALUES
(203, '2017-04-21 02:59:47', '2017-04-20', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '2', '2', '09:45:00', '09:45:00', 0, NULL, NULL),
(204, '2017-04-21 21:16:12', '2017-04-20', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '3', '3', '09:45:00', '04:15:00', 0, NULL, NULL),
(206, '2017-04-21 21:11:21', '2017-04-21', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '23', '32', '04:00:00', '04:00:00', 0, NULL, NULL),
(207, '2017-04-22 02:55:31', '2017-04-21', '09:45:00', NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '3', '3223', '04:15:00', '04:15:00', 0, NULL, NULL),
(208, '2017-04-22 03:01:28', '2017-04-21', '10:00:00', NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '77', '7', '04:15:00', '04:15:00', 1, NULL, NULL),
(209, '2017-04-22 02:08:10', '2017-04-21', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '5', '565', '08:45:00', '09:00:00', 0, NULL, NULL),
(210, '2017-04-22 02:13:15', '2017-04-21', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '', '', '09:00:00', NULL, 0, NULL, NULL),
(211, '2017-04-22 03:00:38', '2017-04-21', '10:00:00', NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '3224323432', '24342323423234', '10:00:00', '10:00:00', 0, NULL, NULL),
(213, '2017-04-22 03:04:05', '2017-04-21', '10:00:00', NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '3', 'dsfasdfz', '10:00:00', '10:00:00', 0, NULL, NULL),
(214, '2017-04-22 03:09:17', '2017-04-21', '10:00:00', NULL, 0, 'Audiencia de Impugnación', 'A', 'De Control', 'Desconocida', 'Desconocido', '10:00:00', '10:00:00', 1, NULL, NULL),
(215, '2017-04-22 03:13:44', '2017-04-21', '10:00:00', NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'ddd', 'DDX', '10:00:00', '10:00:00', 1, NULL, NULL),
(216, '2017-04-24 22:11:27', '2017-04-24', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'a', '33w', '03:00:00', '05:00:00', 0, NULL, NULL),
(217, '2017-04-24 21:45:14', '2017-04-24', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '665', '75', '04:30:00', '04:45:00', 0, NULL, NULL),
(218, '2017-04-24 22:01:03', '2017-04-24', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '6654', '435', '05:00:00', NULL, 1, NULL, NULL),
(219, '2017-04-24 22:09:27', '2017-04-24', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'bvb', 'bb', '05:00:00', NULL, 0, NULL, NULL),
(222, '2017-04-26 05:50:34', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'asdf', '4resf', '12:45:00', NULL, 0, NULL, NULL),
(223, '2017-04-26 09:34:25', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'q', 'qsda', '04:15:00', '04:30:00', 0, NULL, NULL),
(224, '2017-04-26 09:39:47', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', 'dfsdf', 'sdafdc', '04:30:00', '04:30:00', 0, NULL, NULL),
(226, '2017-04-26 09:41:32', '2017-04-26', NULL, NULL, 1, 'Audiencia Intermedia', 'A', 'Exterior', '', 'dsfd', '04:30:00', '04:30:00', 0, NULL, NULL),
(227, '2017-04-26 09:42:21', '2017-04-26', NULL, NULL, 1, 'Audiencia Intermedia', 'A', 'Exterior', '', 'dc', '04:30:00', '04:30:00', 0, NULL, NULL),
(228, '2017-04-26 09:42:57', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '', 'fv', '04:30:00', '04:30:00', 0, NULL, NULL),
(229, '2017-04-26 09:44:41', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '', 'jh', '04:30:00', '04:30:00', 0, NULL, NULL),
(230, '2017-04-26 09:45:17', '2017-04-26', NULL, NULL, 1, 'Audiencia Intermedia', 'A', 'Exterior', '', 'k', '04:45:00', '04:45:00', 0, NULL, NULL),
(231, '2017-04-26 09:46:26', '2017-04-26', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', '', 'adfb', '04:45:00', NULL, 0, NULL, NULL),
(233, '2017-05-03 17:47:57', '2017-05-03', NULL, NULL, 0, 'Audiencia Intermedia', 'A', 'Exterior', ' tvbtt', 'ttrtr', '12:30:00', '12:30:00', 1, '01:15:00', '03:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `elemento`
--

CREATE TABLE IF NOT EXISTS `elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_audiencia` int(11) NOT NULL,
  `id_persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Dumping data for table `elemento`
--

INSERT INTO `elemento` (`id`, `nombre`, `id_audiencia`, `id_persona`) VALUES
(20, '3', 146, NULL),
(21, '2', 148, NULL),
(22, '3', 149, NULL),
(23, '3', 150, NULL),
(24, '2', 151, NULL),
(25, 'César', 152, NULL),
(26, 'César', 152, NULL),
(27, 'César', 153, NULL),
(28, 'w', 156, NULL),
(29, '234', 158, NULL),
(30, 'Ruff0', 159, NULL),
(31, 'César', 159, NULL),
(32, 'César', 160, NULL),
(33, 'César', 161, NULL),
(34, '3', 162, NULL),
(35, 'Ruff0', 163, NULL),
(36, 'César', 164, NULL),
(37, '3', 165, NULL),
(38, 'César', 166, NULL),
(39, '123', 167, NULL),
(40, 'sdf', 168, NULL),
(41, '4', 169, NULL),
(42, '3', 170, NULL),
(43, 'César', 171, NULL),
(44, 'Ruff0', 172, NULL),
(45, 'Ruff0', 173, NULL),
(46, 'César', 174, NULL),
(47, '4', 175, NULL),
(48, 'Ruff0', 176, NULL),
(49, 'César', 177, NULL),
(50, 'a', 178, NULL),
(51, '4', 179, NULL),
(52, '4', 180, NULL),
(53, '2', 181, NULL),
(54, '3', 182, NULL),
(55, '4', 185, NULL),
(56, 'Ruff0', 186, NULL),
(57, '4', 187, NULL),
(58, '5', 188, NULL),
(59, '4', 189, NULL),
(60, 'p', 190, NULL),
(61, '32', 206, NULL),
(62, '3', 206, NULL),
(63, '234234234234234fsd', 211, NULL),
(64, '1', 213, NULL),
(65, '2', 213, NULL),
(66, '3', 213, NULL),
(67, '4', 213, NULL),
(68, '1', 214, NULL),
(69, '2', 214, NULL),
(70, 'DDXD2', 215, NULL),
(71, '3333', 215, NULL),
(72, 'bbv', 219, NULL),
(77, 'nbnb', 216, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `elemento_directorio`
--

CREATE TABLE IF NOT EXISTS `elemento_directorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_tipo_elemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `elemento_directorio`
--

INSERT INTO `elemento_directorio` (`id`, `nombre`, `id_tipo_elemento`) VALUES
(5, 'José', 'Fijo'),
(6, 'César', 'Auxiliar');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1492625023),
('m130524_201442_init', 1492625026),
('m140209_132017_init', 1492668578),
('m140403_174025_create_account_table', 1492668579),
('m140504_113157_update_tables', 1492668584),
('m140504_130429_create_token_table', 1492668585),
('m140830_171933_fix_ip_field', 1492668586),
('m140830_172703_change_account_table_name', 1492668586),
('m141222_110026_update_ip_field', 1492668586),
('m141222_135246_alter_username_length', 1492668587),
('m150614_103145_update_social_account_table', 1492668588),
('m150623_212711_fix_username_notnull', 1492668588),
('m151218_234654_add_timezone_to_profile', 1492668589),
('m160929_103127_add_last_login_at_to_user_table', 1492668589);

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_audiencia` int(11) DEFAULT NULL,
  `inputado` tinyint(1) DEFAULT NULL,
  `hora_salida` time DEFAULT NULL,
  `hora_regreso` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=86 ;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`id`, `nombre`, `id_audiencia`, `inputado`, `hora_salida`, `hora_regreso`) VALUES
(24, '2', 146, NULL, NULL, NULL),
(25, '2', 148, NULL, NULL, NULL),
(26, '3', 149, NULL, NULL, NULL),
(27, '3', 150, NULL, NULL, NULL),
(28, '2', 151, NULL, NULL, NULL),
(29, '42342', 152, NULL, NULL, NULL),
(30, '', 152, NULL, NULL, NULL),
(31, '4', 153, NULL, NULL, NULL),
(32, 'q', 156, NULL, NULL, NULL),
(33, 'w', 156, NULL, NULL, NULL),
(34, 'w', 156, NULL, NULL, NULL),
(35, '234', 158, NULL, NULL, NULL),
(36, 'asdfsa', 159, NULL, NULL, NULL),
(37, '1', 160, NULL, NULL, NULL),
(38, 'César', 161, NULL, NULL, NULL),
(39, 'Ruff0', 161, NULL, NULL, NULL),
(40, '2', 162, NULL, NULL, NULL),
(41, 'César', 163, NULL, NULL, NULL),
(42, 'César', 164, NULL, NULL, NULL),
(43, '3', 165, NULL, NULL, NULL),
(44, '3', 166, NULL, NULL, NULL),
(45, '213', 167, NULL, NULL, NULL),
(46, '23', 168, NULL, NULL, NULL),
(47, 'sdf', 168, NULL, NULL, NULL),
(48, '4', 169, NULL, NULL, NULL),
(49, '3', 170, NULL, NULL, NULL),
(50, '123', 171, NULL, NULL, NULL),
(51, 'César', 172, NULL, NULL, NULL),
(52, 'César', 173, NULL, NULL, NULL),
(53, '3', 174, NULL, NULL, NULL),
(54, '4', 175, NULL, NULL, NULL),
(55, 'César', 176, NULL, NULL, NULL),
(56, 'Ruff0', 177, NULL, NULL, NULL),
(57, '3', 178, NULL, NULL, NULL),
(58, '3', 179, NULL, NULL, NULL),
(59, '4', 180, NULL, NULL, NULL),
(60, '2', 181, NULL, NULL, NULL),
(61, '2', 182, NULL, NULL, NULL),
(62, '3', 185, NULL, NULL, NULL),
(63, 'César', 186, NULL, NULL, NULL),
(64, '3', 187, NULL, NULL, NULL),
(65, '4', 188, NULL, NULL, NULL),
(66, '3', 189, NULL, NULL, NULL),
(67, 'o', 190, NULL, NULL, NULL),
(68, '2', 192, NULL, NULL, NULL),
(69, 'César', 195, NULL, NULL, NULL),
(70, '2', 196, NULL, NULL, NULL),
(71, 'e', 197, NULL, NULL, NULL),
(72, 'w', 200, NULL, NULL, NULL),
(73, '2', 201, NULL, NULL, NULL),
(74, '3', 202, NULL, NULL, NULL),
(75, '2', 203, NULL, NULL, NULL),
(76, '4', 204, NULL, NULL, NULL),
(77, '23', 206, NULL, NULL, NULL),
(78, '432', 207, NULL, NULL, NULL),
(79, '67', 208, NULL, NULL, NULL),
(80, '3342', 209, NULL, NULL, NULL),
(81, 'Desconocido', 214, NULL, NULL, NULL),
(82, 'w', 214, NULL, NULL, NULL),
(83, 'DDXD', 215, NULL, NULL, NULL),
(85, 'bc', 219, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sala`
--

CREATE TABLE IF NOT EXISTS `sala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sala` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_tipo_sala` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sala`
--

INSERT INTO `sala` (`id`, `sala`, `id_tipo_sala`) VALUES
(1, 'A', '1'),
(2, 'B', '1'),
(3, 'C', '2'),
(4, '3', '0'),
(5, '4', 'Exterior');

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_audiencia`
--

CREATE TABLE IF NOT EXISTS `tipo_audiencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tipo_audiencia`
--

INSERT INTO `tipo_audiencia` (`id`, `nombre`) VALUES
(1, 'Audiencia Intermedia'),
(2, 'Audiencia de Impugnación'),
(3, 'Audiencia Intermedia y/o de Procedimiento Abreviado'),
(4, 'Audiencia de Juicio Oral'),
(5, 'Audiencia Inicial'),
(6, 'Audiencia de Solicitud de Suspensión Condicional del Proceso'),
(7, 'Audiencia de Revocación de Suspensión Condicional del Proceso'),
(8, 'Audiencia de Amonestación');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_elemento`
--

CREATE TABLE IF NOT EXISTS `tipo_elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tipo_elemento`
--

INSERT INTO `tipo_elemento` (`id`, `nombre`) VALUES
(1, 'Fijo'),
(2, 'Auxiliar');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_sala`
--

CREATE TABLE IF NOT EXISTS `tipo_sala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_sala` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tipo_sala`
--

INSERT INTO `tipo_sala` (`id`, `tipo_sala`) VALUES
(1, 'Exterior'),
(2, 'De Control');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `status`, `password_reset_token`) VALUES
(1, 'ruff0', 'ruff0@400tres.com', '$2y$13$A8S9EaGQ4GQ9j8DYUGXnPubsFfdi6GY8coup6QB1oFKHsRSfWm6Ne', 'HVS7EkRsOcAMicpFiXuWSmuHMelX_aYv', NULL, NULL, NULL, NULL, 1492675387, 1492675387, 0, 1492835665, 10, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
