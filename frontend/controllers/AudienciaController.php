<?php

namespace frontend\controllers;

use frontend\models\Elemento;
use frontend\models\Persona;
use Yii;
use frontend\models\Audiencia;
use frontend\models\AudienciaSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
class AudienciaController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionReport()
    {
        $searchModel = new AudienciaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        public function actionIndex()
        {
            $searchModel = new AudienciaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionPie()	{
        	$dataProvider = new ActiveDataProvider([
        		'query' => Audiencia::find(),
        	    'pagination' => false
        	]);

        	return $this->render('pie', [
        		'dataProvider' => $dataProvider
        	]);
        }

        public function actionIndextoday()
        {
            $expression = new Expression('NOW()');
            $now = (new \yii\db\Query)->select($expression)->scalar();
            $now=substr($now, 0, 10);
            $searchModel = AudienciaSearch::find()->andFilterWhere(['like', 'fecha', $now]);
            $dataProvider = new ActiveDataProvider(['query' => $searchModel]);
            return $this->render('indextoday', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'now' =>  $now,
            ]);
        }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    public function actionCreate2()
    {
        $model = new Audiencia();
        $modelPersonas = [];
        $modelElementos = [];

        $formPersonas = Yii::$app->request->post('Persona', []);
        foreach ($formPersonas as $i => $formPersona) {
            $modelPersona = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
            $modelPersona->setAttributes($formPersona);
            $modelPersonas[] = $modelPersona;
        }

        $formElementos = Yii::$app->request->post('Elemento', []);
        foreach ($formElementos as $i => $formElemento) {
          $modelElemento = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
          $modelElemento->setAttributes($formElemento);
          $modelElementos[] = $modelElemento;
        }


        //handling if the addRow button has been pressed
        if (Yii::$app->request->post('addRow') == 'true') {
            $model->load(Yii::$app->request->post());
            $modelPersonas[] = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
            return $this->render('create2', [
                'model' => $model,
                'modelPersonas' => $modelPersonas,
                'modelElementos' => $modelElementos

            ]);
        }

        if (Yii::$app->request->post('addRowElemento') == 'true') {
            $model->load(Yii::$app->request->post());
            $modelElementos[] = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
            return $this->render('create2', [
                'model' => $model,
                'modelPersonas' => $modelPersonas,
                'modelElementos' => $modelElementos

            ]);
        }



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          if (Model::validateMultiple($modelPersonas) && ($modelElementos) && $model->validate()) {
              $model->save();
              foreach($modelPersonas as $modelPersona) {
                  $modelPersona->id_audiencia = $model->id;
                  $modelPersona->save();

              }

              foreach($modelElementos as $modelElemento) {
                  $modelElemento->id_audiencia = $model->id;
                  $modelElemento->save();
              }


              return $this->redirect(['view', 'id' => $model->id]);
          }
        }

             return $this->render('create2', [
                'model' => $model,
                'modelPersonas' => $modelPersonas,
                'modelElementos' => $modelElementos
            ]);
        }



          public function actionCreate()
          {
              $model = new Audiencia();
              $modelPersonas = [];
              $modelElementos = [];

              $formPersonas = Yii::$app->request->post('Persona', []);
              foreach ($formPersonas as $i => $formPersona) {
                  $modelPersona = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
                  $modelPersona->setAttributes($formPersona);
                  $modelPersonas[] = $modelPersona;
              }

              $formElementos = Yii::$app->request->post('Elemento', []);
              foreach ($formElementos as $i => $formElemento) {
                $modelElemento = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
                $modelElemento->setAttributes($formElemento);
                $modelElementos[] = $modelElemento;
              }
              //handling if the addRow button has been pressed
              if (Yii::$app->request->post('addRow') == 'true') {
                  $model->load(Yii::$app->request->post());
                  $modelPersonas[] = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
                  return $this->render('create', [
                      'model' => $model,
                      'modelPersonas' => $modelPersonas,
                      'modelElementos' => $modelElementos

                  ]);
              }

              if (Yii::$app->request->post('addRowElemento') == 'true') {
                  $model->load(Yii::$app->request->post());
                  $modelElementos[] = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
                  return $this->render('create', [
                      'model' => $model,
                      'modelPersonas' => $modelPersonas,
                      'modelElementos' => $modelElementos

                  ]);
              }

              if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (Model::validateMultiple($modelPersonas) || ($modelElementos) && $model->validate()) {
                    $model->save();
                    foreach($modelPersonas as $modelPersona) {
                        $modelPersona->id_audiencia = $model->id;
                        $modelPersona->save();

                    }
                    foreach($modelElementos as $modelElemento) {
                        $modelElemento->id_audiencia = $model->id;
                        $modelElemento->save();
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
              }
                   return $this->render('create', [
                      'model' => $model,
                      'modelPersonas' => $modelPersonas,
                      'modelElementos' => $modelElementos
                  ]);
              }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelPersonas = $model->persona;
        $modelElementos = $model->elemento;
        $formPersonas = Yii::$app->request->post('Persona', []);
        $formElementos = Yii::$app->request->post('Elemento', []);


              foreach ($formPersonas as $i => $formPersona) {
                  //loading the models if they are not new
                  if (isset($formPersona['id']) && isset($formPersona['updateType']) && $formPersona['updateType'] != Persona::UPDATE_TYPE_CREATE) {
                      //making sure that it is actually a child of the main model
                      $modelPersona = Persona::findOne(['id' => $formPersona['id'], 'id_audiencia' => $model->id]);
                      $modelPersona->setScenario(Persona::SCENARIO_BATCH_UPDATE);
                      $modelPersona->setAttributes($formPersona);
                      $modelPersonas[$i] = $modelPersona;
                      //validate here if the modelPersona loaded is valid, and if it can be updated or deleted
                  } else {
                      $modelPersona = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
                      $modelPersona->setAttributes($formPersona);
                      $modelPersonas[] = $modelPersona;
                  }

                      }

                foreach ($formElementos as $i => $formElemento) {
                    if (isset($formElemento['id']) && isset($formElemento['updateTypeElemento']) && $formElemento['updateTypeElemento'] != Elemento::UPDATE_TYPE_ELEMENTO_CREATE) {
                        $modelElemento = Elemento::findOne(['id' => $formElemento['id'], 'id_audiencia' => $model->id]);
                        $modelElemento->setScenario(Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE);
                        $modelElemento->setAttributes($formElemento);
                        $modelElementos[$i] = $modelElemento;
                    } else {
                        $modelElemento = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
                        $modelElemento->setAttributes($formElemento);
                        $modelElementos[] = $modelElemento;
                    }
                }


        //handling if the addRow button has been pressed
        if (Yii::$app->request->post('addRow') == 'true') {
            $modelPersonas[] = new Persona(['scenario' => Persona::SCENARIO_BATCH_UPDATE]);
            return $this->render('update', [
                'model' => $model,
                'modelPersonas' => $modelPersonas,
                'modelElementos' => $modelElementos


            ]);
        }
        //handling if the addRow button has been pressed
        if (Yii::$app->request->post('addRowElemento') == 'true') {
            $modelElementos[] = new Elemento(['scenario' => Elemento::SCENARIO_ELEMENTO_BATCH_UPDATE]);
            return $this->render('update', [
                'model' => $model,
                'modelElementos' => $modelElementos,
                'modelPersonas' => $modelPersonas


            ]);
        }
                                if ($model->load(Yii::$app->request->post())) {
                                    if (Model::validateMultiple($modelPersonas) || ($modelElementos) && $model->validate()) {
                                        $model->save();
                                        foreach($modelPersonas as $modelPersona) {
                                            //details that has been flagged for deletion will be deleted
                                            if ($modelPersona->updateType == Persona::UPDATE_TYPE_DELETE) {
                                                $modelPersona->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelPersona->id_audiencia = $model->id;
                                                $modelPersona->save();
                                            }
                                        }

                                        foreach($modelElementos as $modelElemento) {
                                            //details that has been flagged for deletion will be deleted
                                            if ($modelElemento->updateTypeElemento == Elemento::UPDATE_TYPE_ELEMENTO_DELETE) {
                                                $modelElemento->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelElemento->id_audiencia = $model->id;
                                                $modelElemento->save();
                                            }
                                        }
                                        return $this->redirect(['view', 'id' => $model->id]);
                                    }
                                }


        return $this->render('update', [
            'model' => $model,
            'modelPersonas' => $modelPersonas,
            'modelElementos' => $modelElementos
        ]);
      }

     public function actionDelete($id)
     {
         $model = $this->findModel($id);

                   foreach ($model->persona as $modelPersona) {
                       $modelPersona->delete();
                   }

                    foreach ($model->elemento as $modelElemento) {
                        $modelElemento->delete();
                    }


         $model->delete();

         return $this->redirect(['index']);
     }

    protected function findModel($id)
    {
        if (($model = Audiencia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
