<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cereso".
 *
 * @property integer $id
 * @property integer $id_persona
 * @property integer $id_audiencia
 * @property string $hora_salida
 * @property string $hora_regreso
 */
class Cereso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cereso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_audiencia', 'hora_salida'], 'required'],
            [['id_persona', 'id_audiencia'], 'integer'],
            [['hora_salida', 'hora_regreso'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_persona' => 'Id Persona',
            'id_audiencia' => 'Id Audiencia',
            'hora_salida' => 'Hora Salida',
            'hora_regreso' => 'Hora Regreso',
        ];
    }
}
