<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Audiencia;
use kartik\daterange\DateRangeBehavior;

class AudienciaSearch extends Audiencia
{

   public $fecha_range;


    public function rules()
    {
        return [
            [['id', 'cancelado'], 'integer'],
            [['fecha_hora', 'id_sala', 'fecha', 'tipo', 'tipo_audiencia', 'tipo_sala', 'hora_real'], 'safe'],
            // [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['fecha_range'], 'safe']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Audiencia::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //  $query->where('0=1');
            return $dataProvider;
        }
        if(!empty($this->fecha_range) && strpos($this->fecha_range, '-') !== false)
        {
          list($start_date, $end_date) = explode(' - ', $this->fecha_range);
          $query->andFilterWhere(['between', 'fecha', $start_date, $end_date]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'id_sala' => $this->id_sala,
            'tipo' => $this->tipo,
            'fecha'=> $this->fecha,
            'tipo_audiencia' => $this->tipo_audiencia,
            'tipo_sala' => $this->tipo_sala,
            'hora_real' => $this->hora_real,
            'cancelado' => $this->cancelado,
        ]);
        return $dataProvider;
    }
}
