<?php
namespace frontend\models;
use Yii;

class Audiencia extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'audiencia';
    }

    public function rules()
    {
        return [
            [['fecha_hora', 'id_sala' , 'incidencia', 'fecha','tipo_audiencia', 'hora', 'hora_inicio', 'hora_termino', 'tipo', 'hora_salida', 'hora_regreso', 'tipo_audiencia', 'tipo_sala', 'causa', 'oficio'], 'safe'],             [['id_sala', 'hora_inicio', 'tipo' ], 'required'],
            [['cancelado' ], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Folio',
            'fecha_hora' => 'Fecha de Captura',
            'id_sala' => 'Sala',
            'hora_inicio' => 'Hora de Inicio',
            'hora_termino' => 'Hora de Término',
            'tipo' => 'Con / Sin Imputado(s)',
            'hora_salida' => 'Hora Salida del CERESO',
            'hora_regreso' => 'Hora Regreso del CERESO',
            'fecha' => 'Fecha',
            'hora_real' => 'Hora Real de Inicio',
            'cancelado' => 'Cancelado',
            'tipo_audiencia' => 'Tipo de Audiencia',
            'tipo_sala' => 'Tipo de Sala',
            'causa' => 'Causa Penal',
            'oficio' => 'Oficio',
            'fecha_range' => 'Desde - Hasta',
            'incidencia' => 'Incidencias',
        ];
    }

    public function getPersona()
    {
        return $this->hasMany(Persona::className(), ['id_audiencia' => 'id']);
    }

    public function getElemento()
    {
        return $this->hasMany(Elemento::className(), ['id_audiencia' => 'id']);
    }
}
