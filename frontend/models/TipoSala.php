<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tipo_sala".
 *
 * @property integer $id
 * @property string $tipo_sala
 */
class TipoSala extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_sala';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_sala'], 'required'],
            [['tipo_sala'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_sala' => 'Tipo Sala',
        ];
    }
}
