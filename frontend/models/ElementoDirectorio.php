<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "elemento_directorio".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $id_tipo_elemento
 */
class ElementoDirectorio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elemento_directorio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_tipo_elemento'], 'required'],
            [['id_tipo_elemento'], 'string', 'max' => 255],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_tipo_elemento' => 'Tipo Elemento',
        ];
    }
}
