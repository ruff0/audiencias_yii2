<?php

namespace frontend\models;

use Yii;

class Elemento extends \yii\db\ActiveRecord
{


  const UPDATE_TYPE_ELEMENTO_CREATE = 'create';
  const UPDATE_TYPE_ELEMENTO_UPDATE = 'update';
  const UPDATE_TYPE_ELEMENTO_DELETE = 'delete';

  const SCENARIO_ELEMENTO_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeElemento;




      public function getUpdateTypeElemento()
      {
          if (empty($this->_updateTypeElemento)) {
              if ($this->isNewRecord) {
                  $this->_updateTypeElemento = self::UPDATE_TYPE_ELEMENTO_CREATE;
              } else {
                  $this->_updateTypeElemento = self::UPDATE_TYPE_ELEMENTO_UPDATE;
              }
          }

          return $this->_updateTypeElemento;
      }

      public function setUpdateTypeElemento($value)
      {
          $this->_updateTypeElemento = $value;
      }

    public static function tableName()
    {
        return 'elemento';
    }

    public function rules()
    {
        return [
          ['updateTypeElemento', 'required', 'on' => self::SCENARIO_ELEMENTO_BATCH_UPDATE],
        ['updateTypeElemento',
            'in',
            'range' => [self::UPDATE_TYPE_ELEMENTO_CREATE, self::UPDATE_TYPE_ELEMENTO_UPDATE, self::UPDATE_TYPE_ELEMENTO_DELETE],
            'on' => self::SCENARIO_ELEMENTO_BATCH_UPDATE]
        ,
            [['nombre'], 'required'],
            [['id_audiencia'], 'integer'],
            [['id_persona'], 'integer'],

            [['nombre'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_audiencia' => 'Id Audiencia',
            'id_persona'=> 'Id Persona',

        ];
    }



    public function getAudiencia()
          {
              return $this->hasOne(Audiencia::className(), ['id' => 'id_audiencia']);
          }

}
