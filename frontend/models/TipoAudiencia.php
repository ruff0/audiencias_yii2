<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tipo_audiencia".
 *
 * @property integer $id_audiencia
 * @property string $nombre_audiencia
 */
class TipoAudiencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_audiencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Audiencia',
            'nombre' => 'Tipo de Audiencia',
        ];
    }
}
