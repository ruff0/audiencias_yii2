<?php

namespace frontend\models;

use Yii;

class Sala extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sala';
    }

    public function rules()
    {
        return [
            [['sala', 'id_tipo_sala'], 'required'],
            [['sala', 'id_tipo_sala'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sala' => 'Sala',
            'id_tipo_sala' => 'Tipo de Sala',
        ];
    }
}
