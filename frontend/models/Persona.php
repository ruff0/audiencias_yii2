<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property int $id
 * @property int $patient_id
 * @property string $desease_case
 * @property string $persona_relation
 */
class Persona extends \yii\db\ActiveRecord
{

  /**
   * these are flags that are used by the form to dictate how the loop will handle each persona
   */
  const UPDATE_TYPE_CREATE = 'create';
  const UPDATE_TYPE_UPDATE = 'update';
  const UPDATE_TYPE_DELETE = 'delete';

  const SCENARIO_BATCH_UPDATE = 'batchUpdate';


  private $_updateType;

  public function getUpdateType()
  {
      if (empty($this->_updateType)) {
          if ($this->isNewRecord) {
              $this->_updateType = self::UPDATE_TYPE_CREATE;
          } else {
              $this->_updateType = self::UPDATE_TYPE_UPDATE;
          }
      }

      return $this->_updateType;
  }

  public function setUpdateType($value)
  {
      $this->_updateType = $value;
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          ['updateType', 'required', 'on' => self::SCENARIO_BATCH_UPDATE],
          ['updateType',
              'in',
              'range' => [self::UPDATE_TYPE_CREATE, self::UPDATE_TYPE_UPDATE, self::UPDATE_TYPE_DELETE],
              'on' => self::SCENARIO_BATCH_UPDATE]
          ,
          ['id_audiencia', 'required', 'except' => self::SCENARIO_BATCH_UPDATE],


            [['id_audiencia'], 'integer'],
            [['nombre', 'inputado'], 'string', 'max' => 255],
            [['hora_salida', 'hora_regreso'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_audiencia' => 'Id Audiencia',
            'nombre' => 'Përsona',
            'hora_salida' => 'Hora de Salida del Cereso',
            'hora_regreso' => 'Hora de Regreso al Cereso',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAudiencia()
    {
        return $this->hasOne(Audiencia::className(), ['id' => 'id_audiencia']);
    }
}
