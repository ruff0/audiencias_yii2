<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Audiencias v0.2',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-left',
        ],
    ]);
    $menuItems = [
        // ['label' => 'Home', 'url' => ['/site/index']],
        // ['label' => 'About', 'url' => ['/site/about']],
        // ['label' => 'Contact', 'url' => ['/site/contact']],
        // Show users section for administrators only


    ];
    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Audiencias de Hoy', 'url' => ['/audiencia/indextoday']];
        $menuItems[] = ['label' => 'Registro', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Ingreso', 'url' => ['/site/login']];


    } else {
          $menuItems[] = ['label' => 'Reporte', 'url' => ['audiencia/report']];
          $menuItems[] = ['label' => 'Audiencias de Hoy', 'url' => ['audiencia/indextoday']];
          $menuItems[] = ['label' => 'Todas las Audiencias', 'url' => ['audiencia/index']];
          $menuItems[] = ['label' => 'Tipos de Audiencias', 'url' => ['tipo-audiencia/index']];
          $menuItems[] = ['label' => 'Elementos', 'url' => ['elemento-directorio/index']];
          $menuItems[] = ['label' => 'Tipo de Elementos', 'url' => ['tipo-elemento/index']];
          $menuItems[] = ['label' => 'Sala', 'url' => ['sala/index']];
          $menuItems[] = ['label' => 'Tipo de Sala', 'url' => ['tipo-sala/index']];
          // $menuItems[] = ['label' => 'Audiencias de Hoy', 'url' => ['/audiencia/indextoday']];

          $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <!-- <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p> -->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
