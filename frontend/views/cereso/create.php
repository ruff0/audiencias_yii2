<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Cereso */

$this->title = 'Create Cereso';
$this->params['breadcrumbs'][] = ['label' => 'Ceresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cereso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
