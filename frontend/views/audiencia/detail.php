<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\Audiencia;

$this->title = 'Oficio '.$model->oficio;
?>
<div class="audiencia-detail">

  <table class="elementos table">
    <tr>
      <!-- <th>ID</th> -->
      <th>Elemento(s)</th>
    </tr>
    <?php foreach($model->elemento as $Elemento) :?>
      <tr>
        <!-- <td><?= $Elemento->id ?></td> -->
        <td><?= $Elemento->nombre ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <!-- <p> -->
        <!-- <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> -->
        <!-- <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> -->
    <!-- </p> -->
<!--
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'hora_termino',
             'hora_salida',
             'hora_regreso',
             'tipo_audiencia',
             'tipo_sala',
             'causa',
        ],
    ]) ?> -->
    <!-- <table class="personas table"> -->
          <!-- <tr> -->
              <!-- <th>ID</th> -->
              <!-- <th>Persona(s)</th> -->
          <!-- </tr> -->
          <!-- <?php foreach($model->persona as $persona) :?> -->
              <!-- <tr> -->
                  <!-- <td><?= $persona->id ?></td> -->
                  <!-- <td><?= $persona->nombre ?></td> -->
              <!-- </tr> -->
          <!-- <?php endforeach; ?> -->
      <!-- </table> -->
</div>
