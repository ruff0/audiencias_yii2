<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\Audiencia;
use frontend\models\Sala;
use kartik\select2\Select2;
use kartik\date\DatePicker;
?>
<?php $tituloexport = 'Audiencias para celebrarse en '.$now?>

<?php $this->title = $tituloexport?>
<?php $salaData=ArrayHelper::map(Sala::find()->orderBy('id')->asArray()->all(), 'sala', 'sala');?>

<div class="audiencia-indextoday">
    <!-- <h4><?= Html::encode($this->title) ?></h4> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<?php
  $gridColumns = [
  [
      'class'=>'kartik\grid\SerialColumn',
      'contentOptions'=>['class'=>'kartik-sheet-style'],
      'width'=>'36px',
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
  ],

  // [
  //   'attribute'=>'fecha',
  //   'vAlign'=>'middle',
  //   'hAlign'=>'right',
  //   'width'=>'17%',
  //   'pageSummary'=>true,
  //   'filterType'=>GridView::FILTER_DATE,
  //   'filterWidgetOptions' => [
  //                   'options' => ['placeholder' => 'Fecha'],
  //                   'type' => DatePicker::TYPE_COMPONENT_APPEND,
  //                   'pluginOptions' => [
  //                       'autoclose' => true,
  //                       'format' => 'yyyy-mm-dd'
  //                   ],
  //               ],
  // ],

  [
    'attribute'=>'hora_inicio',
    'vAlign'=>'middle',
    'hAlign'=>'right',
    'width'=>'7%',
    'pageSummary'=>true,
  ],

  [
    'pageSummary'=>true,
      'attribute'=>'id_sala',
      'vAlign'=>'middle',
      'hAlign'=>'center',
      'width'=>'80px',
      'filterType'=>GridView::FILTER_SELECT2,
      'filter'=>$salaData,
      'filterWidgetOptions'=>[
                  'pluginOptions' => [
                      'allowClear'=>true,
          ],
      ],
      'filterInputOptions'=>['placeholder'=>'Sala'],
  ],
  [
      'class'=>'kartik\grid\ExpandRowColumn',
      'width'=>'50px',
      'value'=>function ($model, $key, $index, $column) {
          return GridView::ROW_COLLAPSED;
      },
      'detail'=>function ($model, $key, $index, $column) {
          return Yii::$app->controller->renderPartial('detail', ['model'=>$model]);
      },
      'headerOptions'=>['class'=>'kartik-sheet-style'],
      'expandOneOnly'=>true,
      'hiddenFromExport' => false,
  ],
  [
    'class'=>'kartik\grid\BooleanColumn',
    'attribute'=>'tipo',
    'vAlign'=>'middle',
    'trueLabel'=>'Con Inputado(s)',
    'falseLabel'=>'Sin Inputado(s)',
    'falseIcon'=>'<span class="glyphicon glyphicon-remove text-danger"> Sin Inputado(s)</span>',
    'trueIcon'=>'<span class="glyphicon glyphicon-ok text-success"> Con Inputado(s)</span>',
  ],

  ['class' => 'kartik\grid\ActionColumn',
  'template'=>'{view}{update}',
  'headerOptions'=>['class'=>'kartik-sheet-style'],
],
[
  'class'=>'kartik\grid\BooleanColumn',
  'attribute'=>'cancelado',
  'vAlign'=>'middle',
  'trueLabel'=>'No Cancelado',
  'falseLabel'=>'Cancelado',
  'falseIcon'=>'<span class="glyphicon glyphicon-ok text-success"> No Cancelado</span>',
  'trueIcon'=>'<span class="glyphicon glyphicon-remove text-danger"> Cancelado</span>',
],
  ];?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
      'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,

    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'resizableColumns'=>true,
    'pjax'=>true,
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    // 'showPageSummary'=> Html::encode($this->title),
    'panel'=>[
        'type'=>GridView::TYPE_PRIMARY,
        'heading'=>$tituloexport,
        ],
    'persistResize'=>false,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
          ],
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true,
    'toolbar'=> [
          '{export}',
          '{toggleData}',
          ],

    'export'=>[
          // 'fontAwesome' => true,
          'PDF' => [
                  'options' => [
                      'title' => $tituloexport,
                       'subject' => $tituloexport,
                      // 'author' => 'NYCSPrep CIMS',
                      // 'keywords' => 'NYCSPrep, preceptors, pdf'
                  ]
              ],
          ],
    'exportConfig' => [
          GridView::EXCEL => [
            'label' => 'Guardar en XLS',
            'showHeader' => true,
            'filename' => $tituloexport,


          ],
          GridView::PDF => [
            'label' => 'Guardar en PDF',
            'showHeader' => true,
            'showCaption' => true,
            'showPageSummary' => true,
            'showFooter' => true,
            'title' => $tituloexport,
            'options' => ['title' => $tituloexport],
            'config' => ['options' => ['title' => $tituloexport],],
            'filename' => $tituloexport,
          ],
      ]
]);
?>

<?php Pjax::end(); ?></div>
