<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use bupy7\drp\Widget;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;

// use kartik\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\AudienciaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audiencia-searchreport">

    <?php $form = ActiveForm::begin([
        'action' => ['report'],
        'method' => 'get',
    ]); ?>

<div class="col-md-12 row">
  <div class="col-md-5  pull-right">

<?=
 $form->field($model, 'fecha_range', [
  'options'=>['class'=>'drp-container form-group']

])->widget(DateRangePicker::classname(), [
  'useWithAddon'=>false,
  // 'presetDropdown'=>true,
  'presetDropdown'=>true,
  'pluginOptions' =>
  [
   //  'useWithAddon' => true,
   //  'showDropdowns'=>true,
    'singleDatePicker'=>false,
    'locale' => [
                                  'format' => 'Y-M-D',
                                  'separator' => ' - '
                                //  'opens' => 'left'
                             ],
    'autoUpdateInput' => false,
    'cancelLabel' => 'Clear',
   //  'allowClear' => true,
  ]
]);?>
</div>
</div>

<div class =" row">
<!--
    <?=  $form->field($model, 'fecha');?>
 -->

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
      </div>

    <?php ActiveForm::end(); ?>

  </div>
</div>
