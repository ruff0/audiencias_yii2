<?php
use frontend\models\Elemento;
use frontend\models\TipoAudiencia;
use frontend\models\Sala;
use frontend\models\Persona;
use frontend\models\ElementoDirectorio;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
use kartik\date\DatePicker;
// use kartik\date\DateTimePicker;
use kartik\datetime\DateTimePicker;
use janisto\timepicker\TimePicker;
// use kartik\time\TimePicker;
use yii\widgets\Pjax;
// use kartik\widgets\TimePicker;
use kartik\select2\Select2;
?>
<?php $this->registerJs("
    $('.delete-button-persona').click(function() {
        var detail = $(this).closest('.persona');
        var updateType = detail.find('.update-type');
        if (updateType.val() === " . json_encode(Persona::UPDATE_TYPE_UPDATE) . ") {
            updateType.val(" . json_encode(Persona::UPDATE_TYPE_DELETE) . ");
            detail.hide();
        } else {
            detail.remove();
        }
    });
    $('.delete-button-elemento').click(function() {
      var detailelemento = $(this).closest('.elemento');
      var updateTypeElemento = detailelemento.find('.update-type-elemento');
      if (updateTypeElemento.val() === " . json_encode(Elemento::UPDATE_TYPE_ELEMENTO_UPDATE) . ") {
        updateTypeElemento.val(" . json_encode(Elemento::UPDATE_TYPE_ELEMENTO_DELETE) . ");
        detailelemento.hide();
      } else {
        detailelemento.remove();
      }
    });
");
?>
<?php Pjax::begin(); ?>
<div class="audiencia-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false
    ]); ?>
    <div style="display:none">
      <div class="col-md-3">
        <?= $form->field($model, 'fecha_hora')->textInput(['disabled'=>'true']) ?>
      </div>
    </div>
    <div class="col-md-12 row">
    <div class="col-md-3">
      <?= TimePicker::widget([
          'language' => 'es',
          'model' => $model,
          'attribute' => 'fecha',
          'mode' => 'date',
          'inline' => true,
          'clientOptions' => [
              'disabled' => true,
              'dateFormat' => 'yy-mm-dd',
              'onClose' => new \yii\web\JsExpression('function(dateText, inst) { console.log("onClose: " + dateText); }'),
              'onSelect' => new \yii\web\JsExpression('function(dateText, inst) { console.log("onSelect: " + dateText); }'),
          ]
      ]);?>
    </div>
    </div>

    <div class="col-md-3">
      <?= $form->field($model, 'hora_inicio')->widget(\kartik\time\TimePicker::classname(), ['pluginOptions'=> ['showMeridian'=>false]]);?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'hora_real')->widget(\kartik\time\TimePicker::classname(), ['pluginOptions'=> ['showMeridian'=>false]]);?>
    </div>
    <div class="col-md-8">
          <?= $form->field($model, 'cancelado')->widget(SwitchInput::classname(), [
                  // 'disabled' => true,
                  'readonly' => false,
                  'pluginOptions' => [

                     'handleWidth'=>120,
                  'offText' => 'No Cancelado',
                  // 'offColor' => 'danger',
                  'offValue' => '0',
                  'onText' => 'Cancelado',
                  'onColor' => 'danger',
                  'onValue' => '1',
              ],
            ]);?>
          </div>
    <div class="col-md-3">

    <?= $form->field($model, 'id_sala')->widget(Select2::className(), [
      'data' => ArrayHelper::map(Sala::find()->all(), 'id', 'sala'),
      'model' => $model,
      'attribute' => 'id_sala',
      'disabled'=>'true',
      'pluginOptions' => [
                          'allowClear' => true
                        ],
    ]); ?>
  </div>
  <div class="col-md-3">
    <?= $form->field($model, 'tipo_audiencia')->widget(Select2::className(), [
      'data' => ArrayHelper::map(TipoAudiencia::find()->all(), 'id', 'nombre'),
      'model' => $model,
      'disabled' => true,
      'attribute' => 'tipo_audiencia',
      'pluginOptions' => ['allowClear' => true],
    ]); ?>
  </div>
    <div class="col-md-3">

    <?= $form->field($model, 'hora_termino')->widget(\kartik\time\TimePicker::classname(), ['pluginOptions'=> ['showMeridian'=>false]]);?>
  </div>
<td class="row  col-md-8">
    <div class="col-md-8">
          <?= $form->field($model, 'tipo')->widget(SwitchInput::classname(), [
                  // 'disabled' => true,
                  'readonly' => true,
                  'pluginOptions' => [

                     'handleWidth'=>120,
                  'offText' => 'Sin Imputado(s)',

                  'onText' => 'Con Imputado(s)',
              ],
            ]);?>
          </div>
          <div class="col-md-3">
                <?php if ($model->tipo == 0 || $model->tipo == false  || $model->tipo == null){
                        }
                else {
                  echo $form->field($model, 'hora_salida')->widget(\kartik\time\TimePicker::classname(), ['pluginOptions'=> ['showMeridian'=>false]]);
                    echo $form->field($model, 'hora_regreso')->widget(\kartik\time\TimePicker::classname(), ['pluginOptions'=> ['showMeridian'=>false]]);
                } ?>
          </div>
  </td>

        <div class="col-md-12">
          <?= $form->field($model, 'incidencia')->textInput(['maxlength' => true]) ?>
        </div>

    <td class="col-md-3">
            <?php foreach ($modelPersonas as $i => $modelPersona) : ?>
            <div class="row persona persona-<?= $i ?>">
                <div class="col-md-11">
                    <?= Html::activeHiddenInput($modelPersona, "[$i]id") ?>
                    <?= Html::activeHiddenInput($modelPersona, "[$i]updateType", ['class' => 'update-type']) ?>
                    <?php
                    $cie10Data = ArrayHelper::map(ElementoDirectorio::find()->all(), 'id', 'nombre')
                    ?><table>
                        <tr>
                          <td class="col-md-3">
                            <?= $form->field($modelPersona, "[$i]nombre")->label('Imputado')->widget(TypeaheadBasic::classname(), [
                              'data' => $cie10Data,
                              'scrollable' => true,
                              'options' => ['placeholder' => 'Imputado'],
                              // 'disabled'=>'true',
                              'pluginOptions' => ['highlight'=>true],
                            ]);
                            ?>
                          </td>
                          <div style="display:none">
                          <td class="col-md-3">
                                </td>
                              </div>
                        </tr>
                        <div class="col-md-1 warning">
                          <?= Html::button('x', ['class' => 'delete-button-persona btn btn-danger', 'data-target' => "persona-$i"]) ?>
                        </div>
                      </table>
                </div>
            </div>
            <?php endforeach; ?>
            <?php foreach ($modelElementos as $j => $modelElemento) : ?>
                <div class="row warning elemento elemento-<?= $j ?>">
                    <div class="col-md-11 warning">
                        <?= Html::activeHiddenInput($modelElemento, "[$j]id") ?>
                        <?= Html::activeHiddenInput($modelElemento, "[$j]updateTypeElemento", ['class' => 'update-type-elemento']) ?>
                        <?php
                        $elementoData = ArrayHelper::map(ElementoDirectorio::find()->all(), 'id', 'nombre')
                        ?><table>
                            <tr>
                              <td class="col-md-3 warning">
                                <?= $form->field($modelElemento, "[$j]nombre")->label('Elemento')->widget(TypeaheadBasic::classname(), [
                                  'data' => $elementoData,
                                  'scrollable' => true,
                                  'options' => ['placeholder' => 'Elemento'],
                                  // 'disabled'=>'true',
                                  'pluginOptions' => ['highlight'=>true],
                                ]);
                                ?>
                              </td>
                            </tr>
                            <div class="col-md-1 warning">
                              <?= Html::button('x', ['class' => 'delete-button-elemento btn btn-danger', 'data-target' => "elemento-$j"]) ?>
                            </div>
                          </table>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="row" >
              <div class="form-group col-md-12" >
                <?= Html::submitButton('Agregar Imputado', ['name' => 'addRow', 'value' => 'true', 'class' => 'btn btn-info']) ?>
              </div>

              <div class="form-group col-md-12">
                <?= Html::submitButton('Agregar Elemento', ['name' => 'addRowElemento', 'value' => 'true', 'class' => 'btn btn-info']) ?>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
              </div>

    <?php ActiveForm::end(); ?>
</div>
<?php Pjax::end(); ?>
