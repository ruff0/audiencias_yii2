<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\Audiencia;
use frontend\models\Sala;
use frontend\models\TipoSala;
use frontend\models\Persona;
use frontend\models\Elemento;
use kartik\select2\Select2;
use kartik\date\DatePicker;
// use kartik\field\FieldRange;
use kartik\daterange\DateRangePicker;
 // use jino5577\daterangepicker\DateRangePicker;

 ?>
 <?php $tituloexport = 'Reporte de Audiencias '?>

 <?php $this->title = $tituloexport?>
<?php
$fechaData=ArrayHelper::map(Audiencia::find()->orderBy('id')->asArray()->all(), 'fecha', 'fecha');
?>
<?php
$salaData=ArrayHelper::map(Sala::find()->orderBy('id')->asArray()->all(), 'sala', 'sala');
?>
<?php
$tiposalaData=ArrayHelper::map(TipoSala::find()->orderBy('id')->asArray()->all(), 'tipo_sala', 'tipo_sala');
?>
<div class="audiencia-indexreport">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_searchreport', ['model' => $searchModel]); ?>
    <?php
      $gridColumns = [
      [
          'class'=>'kartik\grid\SerialColumn',
          'contentOptions'=>['class'=>'kartik-sheet-style'],

          'width'=>'7%',

          'header'=>'',
          'headerOptions'=>['class'=>'kartik-sheet-style']
      ],
  //   [
  // 'attribute' => 'fecha',
  // 'value' => function ($model)
  // {
  //   if (extension_loaded('intl'))
  //   {
  //     return
  //     $model->fecha;
  //
  //   }
  //   else
  //   {
  //     return date('Y-M-D', $model->fecha);
  //   }
  // },
  // 'headerOptions' => [ 'class' => 'col-md-2' ],
  // 'filter' => DateRangePicker::widget(
  //   [
  //      'model' => $searchModel,
  //      'attribute' => 'fecha_range',
  //      'pluginOptions' =>
  //      [
  //       //  'useWithAddon' => true,
  //         //  'presetDropdown'=>true,
  //       //  'showDropdowns'=>true,
  //       //  'singleDatePicker'=>false,
  //       'locale' => [
  //         'format' => 'Y-M-D',
  //       ],
  //
  //        'autoUpdateInput' => true,
  //       //  'cancelLabel' => 'Clear',
  //       //  'allowClear' => true,
  //      ]
  //   ])
  // ],

  [
    'attribute'=>'fecha',
    'vAlign'=>'middle',
    'hAlign'=>'right',
    'width'=>'27%',
    // 'pageSummary'=>true,
  ],
  [
      'attribute'=>'hora_inicio',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'7%',
      // 'pageSummary'=>true,
    ],
      [
        'attribute'=>'id_sala',
        'vAlign'=>'middle',
        'hAlign'=>'right',
        'width'=>'17%',
        // 'pageSummary'=>true,
      ],

      [
        'attribute'=>'tipo_sala',
        'vAlign'=>'middle',
        'hAlign'=>'right',
        'width'=>'17%',
        // 'pageSummary'=>true,
      ],


      [
          'class'=>'kartik\grid\ExpandRowColumn',
          'width'=>'7%',

          'value'=>function ($model, $key, $index, $column) {
              return GridView::ROW_COLLAPSED;
          },
          'detail'=>function ($model, $key, $index, $column) {
              return Yii::$app->controller->renderPartial('detail', ['model'=>$model]);
          },
          'headerOptions'=>['class'=>'kartik-sheet-style'],
          'expandOneOnly'=>true,
          'hiddenFromExport' => false,
      ],


      [
       'attribute'=>'incidencia',
       'vAlign'=>'middle',
       'hAlign'=>'right',
       'width'=>'17%',
       // 'pageSummary'=>true,
      ],
      // [
      //   'class'=>'kartik\grid\BooleanColumn',
      //   'attribute'=>'tipo',
      //   'vAlign'=>'middle',
      //   'trueLabel'=>'Con Inputado(s)',
      //   'falseLabel'=>'Sin Inputado(s)',
      //   'falseIcon'=>'<span class="glyphicon glyphicon-remove text-danger"> Sin Inputado(s)</span>',
      //   'trueIcon'=>'<span class="glyphicon glyphicon-ok text-success"> Con Inputado(s)</span>',
      // ],
      ];?>
  <?php Pjax::begin(); ?>
   <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => $gridColumns,
          'headerRowOptions'=>['class'=>'kartik-sheet-style'],
          'filterRowOptions'=>['class'=>'kartik-sheet-style'],
          'resizableColumns'=>true,
          'pjax'=>true,
          'bordered'=>true,
          'striped'=>true,
          'condensed'=>true,
          'showPageSummary'=>false,
          'panel'=>[
              'type'=>GridView::TYPE_PRIMARY,
              'heading'=>false,
              ],
          'toolbar'=> [
                '{export}',
                '{toggleData}',
                ],
          'export'=>[
                // 'fontAwesome' => true,
                'PDF' => [
                        'options' => [
                            'title' => $tituloexport,
                             'subject' => $tituloexport,
                            // 'author' => 'NYCSPrep CIMS',
                            // 'keywords' => 'NYCSPrep, preceptors, pdf'
                        ]
                    ],
                ],
          'exportConfig' => [
                GridView::EXCEL => [
                  'label' => 'Guardar en XLS',
                  'showHeader' => true,
                  'filename' => $tituloexport,


                ],
                GridView::PDF => [
                  'label' => 'Guardar en PDF',
                  'showHeader' => true,
                  'showCaption' => true,
                  'showPageSummary' => true,
                  'showFooter' => true,
                  'title' => $tituloexport,
                  'options' => ['title' => $tituloexport],
                  'config' => ['options' => ['title' => $tituloexport],],
                  'filename' => $tituloexport,
                ],
          ]
      ]); ?>
  <?php Pjax::end(); ?></div>
