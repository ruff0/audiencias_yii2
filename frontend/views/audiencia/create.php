<?php
use frontend\models\Elemento;
use frontend\models\TipoAudiencia;
use frontend\models\Sala;
use frontend\models\TipoSala;

use frontend\models\Persona;
use frontend\models\ElementoDirectorio;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
use kartik\date\DatePicker;
// use kartik\date\DateTimePicker;
use kartik\datetime\DateTimePicker;
// use kartik\time\TimePicker;
use janisto\timepicker\TimePicker;
use yii\widgets\Pjax;
// use kartik\widgets\TimePicker;
use kartik\select2\Select2;
?>
<?php $this->registerJs("
    $('.delete-button').click(function() {
        var detail = $(this).closest('.persona');
        var updateType = detail.find('.update-type');
        if (updateType.val() === " . json_encode(Persona::UPDATE_TYPE_UPDATE) . ") {
            updateType.val(" . json_encode(Persona::UPDATE_TYPE_DELETE) . ");
            detail.hide();
        } else {
            detail.remove();
        }

    });

    $('.delete-button-elemento').click(function() {
      var detailelemento = $(this).closest('.elemento');
      var updateTypeElemento = detailelemento.find('.update-type-elemento');
      if (updateTypeElemento.val() === " . json_encode(Elemento::UPDATE_TYPE_ELEMENTO_UPDATE) . ") {
        updateTypeElemento.val(" . json_encode(Elemento::UPDATE_TYPE_ELEMENTO_DELETE) . ");
        detailelemento.hide();
      } else {
        detailelemento.remove();
      }

    });
");
?>
<?php Pjax::begin(); ?>
<div class="audiencia-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false
    ]); ?>
    <td class"col-md-12">

    <div class="col-md-12">
      <?= TimePicker::widget([
          'language' => 'es',
          'model' => $model,
          'attribute' => 'fecha',
          'mode' => 'date',
          'inline' => true,
          'clientOptions' => [
            'dateFormat' => 'yy-mm-dd',
              'onClose' => new \yii\web\JsExpression('function(dateText, inst) { console.log("onClose: " + dateText); }'),
              'onSelect' => new \yii\web\JsExpression('function(dateText, inst) { console.log("onSelect: " + dateText); }'),
          ]
      ]);?>
    </div>
    </td>
  <div class"row col-md-12">

    <div class="col-md-3">
        <?= $form->field($model, 'hora_inicio')->widget(\kartik\time\TimePicker::classname(), [
	'pluginOptions'=> ['showMeridian'=>false]
	]);?>
    </div>
      <div class="col-md-3">
        <?= $form->field($model, 'id_sala')->widget(Select2::className(), [
          'data' => ArrayHelper::map(Sala::find()->all(), 'sala', 'sala'),
          'model' => $model,
          'attribute' => 'id_sala',
          'pluginOptions' => ['allowClear' => true],
        ]); ?>
      </div>
      <div class="col-md-3">
        <?= $form->field($model, 'tipo_audiencia')->widget(Select2::className(), [
          'data' => ArrayHelper::map(TipoAudiencia::find()->all(), 'nombre', 'nombre'),
          'model' => $model,
          'attribute' => 'tipo_audiencia',
          'pluginOptions' => ['allowClear' => true],
        ]); ?>
      </div>

      <div class="col-md-3">
        <?= $form->field($model, 'tipo_sala')->widget(Select2::className(), [
          'data' => ArrayHelper::map(TipoSala::find()->all(), 'tipo_sala', 'tipo_sala'),
          'model' => $model,
          'attribute' => 'tipo_sala',
          'pluginOptions' => ['allowClear' => true],
        ]); ?>
      </div>
</div>
      <div class="col-md-12">

        <?= $form->field($model, 'causa')->textInput(['maxlength' => true]) ?>
      </div>

        <div class="col-md-12">

        <?= $form->field($model, 'oficio')->textInput(['maxlength' => true]) ?>
      </div>

          <div class="col-md-6">
              <?= $form->field($model, 'tipo')->widget(SwitchInput::classname(), [
                      'pluginOptions' => [
                            'onText' => 'Con Imputado(s)',
                            'offText' => 'Sin Imputado(s)',
                            'handleWidth'=> 120,

                      ],
                      'pluginEvents' => [
                             'switchChange.bootstrapSwitch' => 'function(event, state) {
                                 if (state == 0 || state == false  || state == null){
                                   $("#'.Html::getInputId($model, 'hora_salida').'").attr("disabled", false);
                                     $("#'.Html::getInputId($model, 'hora_regreso').'").attr("disabled", false);
                                 }
                                 else {
                                   $("#'.Html::getInputId($model, 'hora_salida').'").attr("disabled", true);
                                     $("#'.Html::getInputId($model, 'hora_regreso').'").attr("disabled", true)
                                 }
                             }',
                      ],
                ]);?>
          </div>

            <?php foreach ($modelPersonas as $i => $modelPersona) : ?>
            <div class="row persona persona-<?= $i ?>">
                <div class="col-md-11">
                    <?= Html::activeHiddenInput($modelPersona, "[$i]id") ?>
                    <?= Html::activeHiddenInput($modelPersona, "[$i]updateType", ['class' => 'update-type']) ?>
                    <?php
                    $personaData = ArrayHelper::map(ElementoDirectorio::find()->all(), 'id', 'nombre')
                    ?><table>
                        <tr>
                          <td class="col-md-3">
                            <?= $form->field($modelPersona, "[$i]nombre")->label('Imputado')->widget(TypeaheadBasic::classname(), [
                              'data' => $personaData,
                              'scrollable' => true,
                              'options' => ['placeholder' => 'Imputado'],
                              'pluginOptions' => ['highlight'=>true],
                            ]);
                            ?>
                          </td>
                        </tr>
                      </table>
                </div>
                <div class="col-md-1">
                    <?= Html::button('x', ['class' => 'delete-button btn btn-danger', 'data-target' => "persona-$i"]) ?>
                </div>
            </div>
            <?php endforeach; ?>
            <?php foreach ($modelElementos as $j => $modelElemento) : ?>
                <div class="row warning elemento elemento-<?= $j ?>">
                    <div class="col-md-11 warning">
                        <?= Html::activeHiddenInput($modelElemento, "[$j]id") ?>
                        <?= Html::activeHiddenInput($modelElemento, "[$j]updateTypeElemento", ['class' => 'update-type-elemento']) ?>
                        <?php
                        $elementoData = ArrayHelper::map(ElementoDirectorio::find()->all(), 'id', 'nombre')
                        ?><table>
                            <tr>
                              <td class="col-md-3 warning">
                                <?= $form->field($modelElemento, "[$j]nombre")->label('Elemento')->widget(TypeaheadBasic::classname(), [
                                  'data' => $elementoData,
                                  'scrollable' => true,
                                  'options' => ['placeholder' => 'Elemento'],

                                  'pluginOptions' => ['highlight'=>true],
                                ]);
                                ?>
                              </td>

                            </tr>
                          </table>
                    </div>
                    <div class="col-md-1 warning">
                        <?= Html::button('x', ['class' => 'delete-button-elemento btn btn-danger', 'data-target' => "elemento-$j"]) ?>
                    </div>
                </div>
            <?php endforeach; ?>

              <div class="row" >
                <div class="form-group col-md-12" >
                  <?= Html::submitButton('Agregar Imputado', ['name' => 'addRow', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                </div>

                <div class="form-group col-md-12">
                  <?= Html::submitButton('Agregar Elemento', ['name' => 'addRowElemento', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                </div>
                <div class="form-group col-md-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
              </div>
    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>
