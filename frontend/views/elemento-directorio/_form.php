<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TipoElemento;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\ElementoDirectorio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="elemento-directorio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="col-md-3">
      <?= $form->field($model, 'id_tipo_elemento')->widget(Select2::className(), [
        'data' => ArrayHelper::map(TipoElemento::find()->all(), 'nombre', 'nombre'),
        'model' => $model,
        'attribute' => 'id_tipo_elemento',
        'pluginOptions' => ['allowClear' => true],
      ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
