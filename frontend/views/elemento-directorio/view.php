<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Elemento Directorios', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="elemento-directorio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'id_tipo_elemento',
        ],
    ]) ?>

</div>
