<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ElementoDirectorio */

$this->title = 'Create Elemento Directorio';
$this->params['breadcrumbs'][] = ['label' => 'Elemento Directorios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elemento-directorio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
