<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TipoSala;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>

<div class="sala-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sala')->textInput(['maxlength' => true]) ?>

    <div class="col-md-3">
      <?= $form->field($model, 'id_tipo_sala')->widget(Select2::className(), [
        'data' => ArrayHelper::map(TipoSala::find()->all(), 'tipo_sala', 'tipo_sala'),
        'model' => $model,
        'attribute' => 'id_tipo_sala',
        'pluginOptions' => ['allowClear' => true],
      ]); ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
